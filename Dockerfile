FROM archlinux/archlinux:base-devel

RUN  echo "[multilib]" >> /etc/pacman.conf \
    && echo "Include = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf \
    && pacman -Sy --noconfirm openssh repo wget sudo lib32-gcc-libs python2-virtualenv python2 ttf-dejavu \
    && wget http://mirrors.ustc.edu.cn/archlinuxcn/x86_64/yay-10.2.2-2-x86_64.pkg.tar.zst \
    && pacman -U ./yay-10.2.2-2-x86_64.pkg.tar.zst --noconfirm \
    && rm ./yay-10.2.2-2-x86_64.pkg.tar.zst \
    && useradd -m -g users -G wheel yay \
    && sed -i 's/^#\s*\(%wheel\s*ALL=(ALL)\s*NOPASSWD:\s*ALL\)/\1/' /etc/sudoers \
    && sudo -u yay mkdir /home/yay/.gnupg \
    && sudo -u yay echo "hkp-cacert /usr/share/gnupg/sks-keyservers.netCA.pem" > /home/yay/.gnupg/dirmngr.conf \
    && sudo -u yay yay -S lineageos-devel --noconfirm \
    && pacman -Scc
